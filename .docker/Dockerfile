FROM		ubuntu:20.04

RUN		apt-get update \
		&& apt-get install -y \
			apt-transport-https \
			ca-certificates \
		&& apt-get clean \
		&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD		--chown=_apt:root https://packaging.gitlab.io/signal-cli/gpg.key /etc/apt/trusted.gpg.d/morph027-signal-cli.asc

RUN		echo "deb https://packaging.gitlab.io/signal-cli signalcli main" > /etc/apt/sources.list.d/morph027-signal-cli.list

RUN		apt-get update \
		&& apt-get -y dist-upgrade \
		&& apt-get install --no-install-recommends -y \
			signal-cli-native \
			signal-cli-dbus-service \
			dbus \
			tini \
			runit \
			python3-pip \
			python3-pydbus \
			python3-dbus \
			python3-magic \
			python3-yaml \
			git \
		&& apt-get autoremove -y \
		&& apt-get clean \
		&& rm -rf /var/lib/apt/lists/*

ENV		DBUS_SESSION_BUS_ADDRESS "unix:path=/var/run/dbus/system_bus_socket"

ENV		SIGNAL_CLI_DBUS_REST_API_ACCESS_LOG "False"

ENV		SIGNAL_CLI_DBUS_REST_API_DEBUG "False"

ENTRYPOINT	["/usr/bin/tini", "--"]

CMD		["/usr/bin/runsvdir", "/etc/service"]

RUN		ln -s /usr/bin/sv /etc/init.d/signal-cli \
		&& ln -s /usr/bin/sv /etc/init.d/system-dbus

COPY		.docker/service/ /etc/service/

ADD		--chown=signal-cli:signal-cli .docker/service-signal-cli /etc/service-signal-cli

RUN		pip3 install --no-cache-dir sanic

COPY		. /tmp/signal-cli-dbus-rest-api/

RUN		pip3 install --no-cache-dir /tmp/signal-cli-dbus-rest-api

VOLUME		["/var/lib/signal-cli"]
