#!/bin/bash

set -e

cat > ~/.pypirc <<EOF
[pypi]
  username = __token__
  password = ${PYPI_TOKEN}
EOF

if [[ -n "${CI}" ]]; then
  sed -i 's,^__version__ = .*,__version__ = "'"${CI_COMMIT_TAG}"'",' signal_cli_dbus_rest_api/version/__init__.py
fi

apk --no-cache --quiet --no-progress add \
  py3-pip \
  py3-twine
pip3 install --upgrade build
python3 -m build
twine upload dist/*"${CI_COMMIT_TAG}"*
