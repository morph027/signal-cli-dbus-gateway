# Development

## Docker

### Create local test image

```bash
CI_REGISTRY=registry.gitlab.com CI_PROJECT_NAMESPACE=morph027 CI_PROJECT_NAME=signal-cli-dbus-rest-api IMAGE_VARIANT=signal-cli-dbus-rest-api CI_COMMIT_REF_SLUG=sanic-extensions PLATFORMS=linux/amd64 BUILDX_ARGS="--no-cache" .gitlab-ci/release-docker.sh
```
